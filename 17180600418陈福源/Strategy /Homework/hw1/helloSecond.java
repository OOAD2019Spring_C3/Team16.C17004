package hellosecond;

interface takeOffCharacteristics{
    String takeOff();
}
class VerticalTakeOff implements takeOffCharacteristics{
    public String takeOff() {
        return "VerticalTakeOff";
    }
}
class LongDistanceTakeOff implements takeOffCharacteristics{
    public String takeOff() {
        return "LongDistanceTakeOff";
    }
}

interface flightCharacteristics{
    String flight();
}
class SubSonicFly implements  flightCharacteristics{
    public String flight() {
        return "SubSonicFly";
    }
}
class SuperSonicFly implements flightCharacteristics{
    public String flight() {
        return "SuperSonicFly";
    }
}
abstract class Aircraft{
    private takeOffCharacteristics tocb;
    private flightCharacteristics fc;
    private String type;
    public void setType(String type){
        this.type = type;
    }
    public void setTocb(takeOffCharacteristics tocb) {
        this.tocb = tocb;
    }
    public void setFc(flightCharacteristics fc){
        this.fc = fc;
    }
    public void desc(){
        System.out.println(type + ' ' + tocb.takeOff() + ' ' + fc.flight());
    }
}
class Helicopter extends Aircraft{
    Helicopter(){
        super.setType("Helicopter");
        super.setTocb(new VerticalTakeOff());
        super.setFc(new SubSonicFly());
    }
}
class AirPlane extends Aircraft{
    AirPlane(){
        super.setType("Helicopter");
        super.setTocb(new LongDistanceTakeOff());
        super.setFc(new SubSonicFly());
    }
}
class Fighter extends Aircraft{
    Fighter(){
        super.setType("Fighter");
        super.setTocb(new LongDistanceTakeOff());
        super.setFc(new SuperSonicFly());
    }
}
class Harrier extends Aircraft{
    Harrier(){
        super.setType("Harrier");
        super.setTocb(new VerticalTakeOff());
        super.setFc(new SuperSonicFly());
    }
}
public class helloSecond{
    public static void main(String[] xx){
        AirPlane ap = new AirPlane();
        ap.desc();
        Harrier hp = new Harrier();
        hp.desc();
    }
}
